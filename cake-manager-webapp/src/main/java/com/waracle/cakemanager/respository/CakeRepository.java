package com.waracle.cakemanager.respository;

import com.waracle.cakemanager.model.Cake;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CakeRepository {

    private final List<Cake> cakes = new ArrayList<>();

    public CakeRepository() {
        final Cake vannila = new Cake("Vannila", "Vannila Cakes are amazing");
        final Cake fruitCake = new Cake("FruitCake",  "Fruits Cakes contains dry fruits");
        final Cake torte = new Cake("Torte",  "Torte Cakes so nice");
        cakes.add(torte);
        cakes.add(vannila);
        cakes.add(fruitCake);
    }

    public List<Cake> getCakes() {
        return cakes;
    }
}
