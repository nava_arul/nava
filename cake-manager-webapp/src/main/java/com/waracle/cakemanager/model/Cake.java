package com.waracle.cakemanager.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.Objects;

public class Cake extends ResourceSupport {


    private String title;
    private String description;

    public Cake() {
    }

    @JsonCreator
    public Cake( @JsonProperty("title") String title, @JsonProperty("description") String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Cake cake = (Cake) o;
        return Objects.equals(title, cake.title) &&
                Objects.equals(description, cake.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), title, description);
    }

    @Override
    public String toString() {
        return "Cake{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
