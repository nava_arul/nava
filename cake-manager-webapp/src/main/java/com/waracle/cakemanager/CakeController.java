package com.waracle.cakemanager;


import com.waracle.cakemanager.model.Cake;
import com.waracle.cakemanager.respository.CakeRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller
public class CakeController {

    @Autowired
    private CakeRepository cakeRepository;

    @GetMapping("/cakes")
    public String displayCakes(Model model) {
        model.addAttribute("cakes", cakeRepository.getCakes());
        return "cakes";
    }

    @RequestMapping(value = "/cakes/download", method = RequestMethod.GET)
    public HttpEntity<List<Cake>> allCakes() {

        final List<Cake> cakes = cakeRepository.getCakes();
        for (Cake cake : cakes) {
            cake.add(linkTo(methodOn(CakeController.class).byTitle(cake.getTitle())).withSelfRel());
        }
        return new ResponseEntity<>(cakes, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/cakes/{title}")
    public HttpEntity<Cake> byTitle(@PathVariable String title) {

        for (Cake cake : cakeRepository.getCakes()) {
            if (StringUtils.equalsIgnoreCase(title, cake.getTitle())) {
                return new ResponseEntity<Cake>(cake, HttpStatus.OK);
            }
        }
        return new ResponseEntity<Cake>(HttpStatus.NOT_FOUND);
    }


}
