package com.waracle.cakemgr;

import com.waracle.cakemanager.model.Cake;
import com.waracle.cakemanager.respository.CakeRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CakeRepositoryTest {

    private CakeRepository cakeRepository;

    @Before
    public void setup(){
        cakeRepository = new CakeRepository();
    }

    @Test
    public void testGetCakes(){
        List<Cake> cakes = cakeRepository.getCakes();
        assertNotNull(cakes);
        assertEquals(3, cakes.size());
    }
}
