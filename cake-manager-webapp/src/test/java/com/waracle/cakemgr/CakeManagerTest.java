package com.waracle.cakemgr;

import com.waracle.cakemanager.CakeController;
import com.waracle.cakemanager.SpringBootRunner;
import com.waracle.cakemanager.model.Cake;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootRunner.class)
@AutoConfigureMockMvc
public class CakeManagerTest
{

    @Autowired
    private CakeController controller;


    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testBean() throws Exception {
        Assert.assertNotNull(controller);
        HttpEntity<List<Cake>> listHttpEntity = controller.allCakes();
        Assert.assertNotNull(listHttpEntity);

    }

    @Test
    public void testJsonResponse() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/cakes/download")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
